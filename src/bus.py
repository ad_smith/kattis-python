tests = int(input())
for i in range(tests):
    n = int(input())
    passengers = 1
    for j in range(n - 1):
        passengers *= 2
        passengers += 1
    print(passengers)
