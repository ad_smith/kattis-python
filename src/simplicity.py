import sys
from collections import Counter


s = sys.stdin.readline().strip()
c = Counter(s)

if len(c.items()) > 2:
    most_common = c.most_common(2)
    print(len(s) - most_common[0][1] - most_common[1][1])
else:
    print(0)

