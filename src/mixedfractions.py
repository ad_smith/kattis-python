import sys


for line in sys.stdin:
    n, d = map(int, line.split())
    if n == 0 and d == 0:
        break
    w = n // d
    print(w, n - w * d, '/', d)
