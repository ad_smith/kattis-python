import collections


m, n = [int(x) for x in input().split()]

# Build a dict containing the letters and direct translations
d = collections.defaultdict(list)
for _ in range(m):
    arr = input().split()
    d[arr[0]].append(arr[1])

# Now add to that dict by applying repeated translations until getting a final translation mapping
for letter in list(d):
    possible_translations = []
    letters_to_check = d[letter]
    while len(letters_to_check) > 0:
        next = letters_to_check.pop()
        if next not in possible_translations:
            possible_translations.append(next)
            letters_to_check.extend(d[next])
    d[letter] = possible_translations

# Now check if each word can be translated
for _ in range(n):
    before, after = input().split()
    if len(before) != len(after):
        print("no")
        continue
    for i in range(len(before)):
        if before[i] != after[i] and after[i] not in d[before[i]]:
            print("no")
            break
    else:
        print("yes")
