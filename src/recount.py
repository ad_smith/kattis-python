import sys
from collections import defaultdict


votes = defaultdict(int)
while True:
    vote = sys.stdin.readline().strip()
    if vote == '***':
        break
    votes[vote] += 1

ranked = sorted(votes, key=votes.get, reverse=True)

if votes[ranked[0]] == votes[ranked[1]]:
    print('Runoff!')
else:
    print(ranked[0])
