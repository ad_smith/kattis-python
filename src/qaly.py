import sys


qaly = 0.0
n = int(sys.stdin.readline())
for i in range(n):
    arr = sys.stdin.readline().split()
    q = float(arr[0])
    y = float(arr[1])
    qaly = qaly + q * y

print(qaly)
