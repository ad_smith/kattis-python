import sys


def area(x1, y1, x2, y2, x3, y3):
    return abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2)


x1, y1 = map(int, sys.stdin.readline().split())
x2, y2 = map(int, sys.stdin.readline().split())
x3, y3 = map(int, sys.stdin.readline().split())

total_area = area(x1, y1, x2, y2, x3, y3)
print(total_area)

trees = 0
n = int(sys.stdin.readline())
for _ in range(n):
    # If the area of 3 subtriangles match the overall area then the point is in
    # the main triangle
    a, b = map(int, sys.stdin.readline().split())
    a1 = area(a, b, x2, y2, x3, y3)
    a2 = area(x1, y1, a, b, x3, y3)
    a3 = area(x1, y1, x2, y2, a, b)
    if total_area == a1 + a2 + a3:
        trees += 1
print(trees)
