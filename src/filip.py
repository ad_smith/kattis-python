import sys


def reverse(n):
    low = n % 10
    mid = (n // 10) % 10
    high = n // 100
    return low * 100 + mid * 10 + high


a, b = map(int, sys.stdin.readline().split())
ar = reverse(a)
br = reverse(b)
if ar > br:
    print(ar)
else:
    print(br)
