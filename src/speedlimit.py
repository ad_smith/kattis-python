import sys


while True:
    n = int(sys.stdin.readline())
    if n == -1:
        exit(0)
    distance = 0
    time = 0
    for i in range(n):
        arr = sys.stdin.readline().split()
        duration = int(arr[1]) - time
        distance += duration * int(arr[0])
        time += duration
    print(f"{distance} miles")

