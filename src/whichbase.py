import sys
import re


p = int(sys.stdin.readline())
for _ in range(p):
    k, n = sys.stdin.readline().split()
    if re.match('^[0-7]+$', n):
        o = int(n, 8)
    else:
        o = 0
    print(k, o, int(n, 10), int(n, 16))
