import sys


scales = {
    'A#': 'Bb',
    'C#': 'Db',
    'D#': 'Eb',
    'F#': 'Gb',
    'G#': 'Ab'
}
for k in list(scales.keys()):
    scales[scales[k]] = k

for i, line in enumerate(sys.stdin):
    splt = line.split()
    if splt[0] in scales:
        print(f'Case {i + 1}: {scales[splt[0]]} {splt[1]}')
    else:
        print(f'Case {i + 1}: UNIQUE')
