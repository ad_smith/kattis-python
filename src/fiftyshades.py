import sys


n = int(sys.stdin.readline())
sessions = 0
for _ in range(n):
    s = sys.stdin.readline().lower()
    if 'rose' in s or 'pink' in s:
        sessions += 1
if sessions > 0:
    print(sessions)
else:
    print('I must watch Star Wars with my daughter')
