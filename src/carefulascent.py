x, y = [int(n) for n in input().split()]
n = int(input())

for _ in range(n):
    arr = [float(f) for f in input().split()]
    shield_distance = arr[1] - arr[0]
    y += shield_distance * arr[2] - shield_distance

print(x / y)

