import sys


expected = [1, 1, 2, 2, 2, 8]
pieces = map(int, sys.stdin.readline().split())
diff = [str(a - b) for a, b in zip(expected, pieces)]
print(' '.join(diff))
