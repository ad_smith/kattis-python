import sys


n = int(sys.stdin.readline())

print("Alice" if n % 2 == 1 else "Bob")
