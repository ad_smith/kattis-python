import sys


s = set()
for word in sys.stdin.readline().split():
    if word in s:
        print('no')
        break
    else:
        s.add(word)
else:
    print('yes')
