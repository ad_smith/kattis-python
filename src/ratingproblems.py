import sys


n, k = map(int, sys.stdin.readline().split())
total = 0
for rating in sys.stdin:
    total += int(rating)
low = (total + -3 * (n - k)) / n
high = (total + 3 * (n - k)) / n
print(low, high)
