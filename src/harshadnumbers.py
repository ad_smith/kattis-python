import sys


def sum_digits(n):
    s = 0
    while n:
        s += n % 10
        n //= 10
    return s


n = int(sys.stdin.readline())
while True:
    if n % sum_digits(n) == 0:
        print(n)
        break
    n += 1
