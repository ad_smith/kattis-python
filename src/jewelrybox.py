"""
See these pages for an explanation of the math formula:
http://mathforum.org/library/drmath/view/53560.html
http://mathforum.org/library/drmath/view/53732.html
Given V(h) = (x - 2) * (y - 2) * h,
The maximum value for V(h) would be when the derivative V'(h) = 0
"""
import sys
import math


n = range(int(sys.stdin.readline()))
for _ in n:
    x, y = map(int, sys.stdin.readline().split())
    h = (x + y - math.sqrt(x ** 2 + y ** 2 - x * y)) / 6
    print(h * (x - 2 * h) * (y - 2 * h))

