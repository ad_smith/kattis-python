import sys


lst = list(map(int, sys.stdin.readline().split()))
while True:
    swapped = False
    for i in range(1, len(lst)):
        if lst[i - 1] > lst[i]:
            tmp = lst[i]
            lst[i] = lst[i - 1]
            lst[i - 1] = tmp
            print(' '.join([str(x) for x in lst]))
            swapped = True
    if not swapped:
        break
