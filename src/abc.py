lst = [int(x) for x in input().split()]
order = input()

lst.sort()
res = []
for s in order:
    res.append(lst[ord(s) - ord('A')])
print(" ".join([str(x) for x in res]))
