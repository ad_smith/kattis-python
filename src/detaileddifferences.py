import sys


n = int(sys.stdin.readline())
for _ in range(n):
    a = sys.stdin.readline().strip()
    b = sys.stdin.readline().strip()
    print(a)
    print(b)
    c = ['.' for _ in a]
    for i in range(len(a)):
        if a[i] != b[i]:
            c[i] = '*'
    print(''.join(c))
