import sys


n = int(sys.stdin.readline())
for _ in range(n):
    x = int(sys.stdin.readline())
    if x % 2 == 0:
        print(x, 'is even')
    else:
        print(x, 'is odd')
