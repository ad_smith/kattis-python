import sys


n = int(sys.stdin.readline())

for _ in range(n):
    k = int(sys.stdin.readline())
    name = sys.stdin.readline().strip()
    items = set()
    for _ in range(k):
        items.add(sys.stdin.readline().strip())
    if 'pancakes' in items and 'pea soup' in items:
        print(name)
        break
else:
    print('Anywhere is fine I guess')
