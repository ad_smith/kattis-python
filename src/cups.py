import sys


n = int(sys.stdin.readline())
cups = []
for _ in range(n):
    lst = sys.stdin.readline().split()
    if lst[0][0].isdigit():
        cups.append((int(lst[0]) // 2, lst[1]))
    else:
        cups.append((int(lst[1]), lst[0]))

cups.sort(key=lambda x: x[0])
for cup in cups:
    print(cup[1])
