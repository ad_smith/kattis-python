import sys


set_count = 1
while True:
    n = int(sys.stdin.readline())
    if n == 0:
        break
    names = []
    for _ in range(n):
        names.append(sys.stdin.readline().strip())
    print(f'SET {set_count}')
    set_count += 1
    first_half = []
    second_half = []
    for i in range(len(names)):
        if i % 2 == 1:
            second_half.append(names[i])
        else:
            first_half.append(names[i])
    final_list = [*first_half, *reversed(second_half)]
    for name in final_list:
        print(name)
