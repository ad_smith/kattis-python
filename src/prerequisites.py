import sys


while True:
    line = sys.stdin.readline().strip()
    if line == '0':
        break
    k, m = map(int, line.split())
    courses = sys.stdin.readline().split()
    met_prerequisites = True
    for _ in range(m):
        allowed_courses = sys.stdin.readline().split()
        from_category = 0
        for course in courses:
            if course in allowed_courses:
                from_category += 1
        if from_category < int(allowed_courses[1]):
            met_prerequisites = False

    if met_prerequisites:
        print('yes')
    else:
        print('no')
