import sys
from collections import defaultdict


d = defaultdict(bool)
sys.stdin.readline()
for line in sys.stdin:
    action, name = line.strip().split()
    if action == 'entry' and d[name]:
        print(name, 'entered', '(ANOMALY)')
        d[name] = True
    elif action == 'entry':
        print(name, 'entered')
        d[name] = True
    elif action == 'exit' and not d[name]:
        print(name, 'exited', '(ANOMALY)')
        d[name] = False
    else:
        print(name, 'exited')
        d[name] = False
