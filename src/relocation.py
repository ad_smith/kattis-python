n, q = map(int, input().split())
locations = [int(i) for i in input().split()]

for _ in range(q):
    op, a, b = map(int, input().split())
    if op == 1:
        locations[a - 1] = b
    else:
        print(abs(locations[a - 1] - locations[b - 1]))
