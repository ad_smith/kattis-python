import sys


n = int(sys.stdin.readline())
length = 0
for _ in range(n):
    length += int(sys.stdin.readline()) - 1
print(length + 1)
