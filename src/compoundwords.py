import sys


words = []
for line in sys.stdin:
    words.extend(line.split())
s = set()
for w1 in words:
    for w2 in words:
        if w1 != w2:
            s.add(w1 + w2)
print('\n'.join(sorted(s)))
