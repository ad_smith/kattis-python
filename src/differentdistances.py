while True:
    arr = input().split()
    if len(arr) == 1:
        break
    x1, y1, x2, y2, p = map(float, arr)
    distance = (abs(x1 - x2) ** p + abs(y1 - y2) ** p) ** (1 / p)
    print(distance)
