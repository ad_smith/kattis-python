import sys


t = int(sys.stdin.readline())
for i in range(t):
    print(f'Recipe # {i + 1}')
    r, p, d = map(int, sys.stdin.readline().split())
    ingredients = []
    scaling_factor = d / p

    for _ in range(r):
        name, weight, percentage = sys.stdin.readline().split()
        weight = float(weight)
        percentage = float(percentage)
        if percentage == 100.0:
            base_weight = weight
        ingredients.append((name, weight, percentage))
    base_weight *= scaling_factor
    for ingredient in ingredients:
        print(ingredient[0], ingredient[2] * base_weight / 100.0)
    print('-' * 40)
