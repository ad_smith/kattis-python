import sys


def transpose(lines):
    transposed = []
    for i in range(len(lines[0])):
        new_line = ''.join([line[i] for line in lines])
        transposed.append(new_line)
    return transposed


def find_best(lines):
    best = '~'
    for line in lines:
        for word in line.split('#'):
            if len(word) > 1 and word < best:
                best = word
    return best


r, c = map(int, sys.stdin.readline().split())
puzzle = []
for _ in range(r):
    line = sys.stdin.readline().strip()
    puzzle.append(line)

print(min(find_best(puzzle), find_best(transpose(puzzle))))
