import sys
import math


for line in sys.stdin:
    a, b, c = sorted(map(int, line.split()))
    if a == b == c == 0:
        break
    if a ** 2 + b ** 2 == c ** 2:
        print('right')
    else:
        print('wrong')
