import sys


n, p = map(int, sys.stdin.readline().split())
profit = list(map(lambda x: int(x) - p, sys.stdin.readline().split()))

max_profit = 0
cur_profit = 0
for x in profit:
    cur_profit += x
    max_profit = max(cur_profit, max_profit)
    cur_profit = max(cur_profit, 0)
print(max_profit)
