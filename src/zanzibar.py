import sys


n = int(sys.stdin.readline())

for _ in range(n):
    turtles_on_zanzibar = 1
    not_from_zanzibar = 0
    turtles = map(int, sys.stdin.readline().split())
    for i in turtles:
        cnt = i - turtles_on_zanzibar * 2
        if cnt > 0:
            not_from_zanzibar += cnt
        turtles_on_zanzibar = i
    print(not_from_zanzibar)
