import sys


n, p, q = map(int, sys.stdin.readline().split())
if ((p + q) // n) % 2 == 0:
    print('paul')
else:
    print('opponent')
