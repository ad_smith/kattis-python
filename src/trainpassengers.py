import sys


max_capacity, n = map(int, sys.stdin.readline().split())
on_train = 0
for _ in range(n):
    left_train, entered_train, stay = map(int, sys.stdin.readline().split())
    if left_train > on_train:
        print('impossible')
        break
    on_train += entered_train - left_train
    if on_train > max_capacity:
        print('impossible')
        break
    if on_train < max_capacity and stay > 0:
        print('impossible')
        break
else:
    if on_train > 0:
        print('impossible')
    else:
        print('possible')
