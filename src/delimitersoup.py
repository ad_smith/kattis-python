import sys
from collections import deque


sys.stdin.readline()
s = sys.stdin.readline().strip()
expected = {
    '{': '}',
    '(': ')',
    '[': ']'
}
d = deque()
for i, ch in enumerate(s):
    if ch == '{' or ch == '[' or ch == '(':
        d.append(ch)
    elif ch == ' ':
        continue
    else:
        if len(d) == 0 or ch != expected[d.pop()]:
            print(ch, i)
            break
else:
    print('ok so far')
