import sys
import math


p = int(sys.stdin.readline())
for i in range(p):
    n = int(sys.stdin.readline().split()[1])
    s1 = math.floor(n * (n + 1) / 2)
    s2 = n ** 2
    s3 = math.floor(n * (n + 1))
    print(i + 1, s1, s2, s3)
