import sys


def minutes(str):
    arr = str.replace(":", " ").split()
    h = int(arr[0])
    if h == 12 and arr[2] == 'a.m.':
        h -= 12
    elif h < 12 and arr[2] == 'p.m.':
        h += 12
    return 60 * h + int(arr[1])


count = 0
while True:
    n = int(sys.stdin.readline())
    if n == 0:
        break
    if count > 0:
        print()
    count += 1
    arr = []
    for i in range(n):
        arr.append(sys.stdin.readline().strip())
    arr.sort(key=minutes)
    for s in arr:
        print(s)

