import sys
import collections


input_args = sys.stdin.readline().split()
participants = int(input_args[0])
points_required = int(input_args[1])
points = collections.defaultdict(int)
winners = collections.defaultdict(bool)
for i in range(participants):
    sys.stdin.readline()
for line in sys.stdin:
    arr = line.split()
    points[arr[0]] += int(arr[1])
    if points[arr[0]] >= points_required and not winners[arr[0]]:
        print(f"{arr[0]} wins!")
        winners[arr[0]] = True

if not True in winners.values():
    print("No winner!")

