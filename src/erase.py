n = int(input())
before = input()
after = input()

for b, a in zip(before, after):
    b = int(b)
    a = int(a)
    if n % 2 == 1 and b == a or n % 2 == 0 and b != a:
        print("Deletion failed")
        break
else:
    print("Deletion succeeded")


