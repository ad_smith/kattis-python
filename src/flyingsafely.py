import sys


tests = int(sys.stdin.readline())
for _ in range(tests):
    cities, pilots = map(int, sys.stdin.readline().split())
    for _ in range(pilots):
        sys.stdin.readline()
    print(cities - 1)
