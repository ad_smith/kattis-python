import sys


n = int(sys.stdin.readline())
for _ in range(n):
    gnomes = list(map(int, sys.stdin.readline().split()))[1:]
    for i in range(len(gnomes) - 1):
        if gnomes[i] != gnomes[i + 1] - 1:
            print(i + 2)
            break
