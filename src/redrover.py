s = input()
shortest = len(s)
for i in range(len(s)):
    for j in range(i + 1, len(s)):
        ss = s[i:j]
        if ss in s[j:]:
            min_chars = len(s) - len(ss) * s.count(ss) + s.count(ss) + len(ss)
            shortest = min(shortest, min_chars)
        else:
            break

print(shortest)
