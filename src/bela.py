import sys


hands, suit = sys.stdin.readline().strip().split()
hands = int(hands)

score = 0
for _ in range(hands * 4):
    card = sys.stdin.readline().strip()
    if card[0] == 'A':
        score += 11
    elif card[0] == 'K':
        score += 4
    elif card[0] == 'Q':
        score += 3
    elif card == f'J{suit}':
        score += 20
    elif card[0] == 'J':
        score += 2
    elif card[0] == 'T':
        score += 10
    elif card == f'9{suit}':
        score += 14

print(score)
