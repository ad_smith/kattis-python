import sys


vowels = ['a', 'e', 'i', 'o', 'u', 'y']

for line in sys.stdin:
    words = line.split()
    translated_words = []
    for word in words:
        vowel_index = 999
        for vowel in vowels:
            index = word.find(vowel)
            if index >= 0:
                vowel_index = min(vowel_index, index)
        if vowel_index == 0:
            translated_words.append(f'{word}yay')
        else:
            translated_words.append(f'{word[vowel_index:]}{word[0:vowel_index]}ay')
    print(' '.join(translated_words))
