import sys
sys.setrecursionlimit(100000)


n = int(sys.stdin.readline())
strings = ['' for _ in range(n)]
ops = [[] for _ in range(n)]

for i in range(n):
    line = sys.stdin.readline().strip()
    strings[i] = line

a = 0
for _ in range(n - 1):
    a, b = map(int, sys.stdin.readline().split())
    a -= 1
    b -= 1
    ops[a].append(b)


def r(k):
    print(strings[k], end='')
    for op in ops[k]:
        r(op)


r(a)
