import sys
from collections import defaultdict


while True:
    common = 0
    n, m = map(int, sys.stdin.readline().split())
    if n == 0:
        break
    d = defaultdict(int)
    for _ in range(n):
        d[int(sys.stdin.readline())] += 1
    for _ in range(m):
        if int(sys.stdin.readline()) in d:
            common += 1
    print(common)
