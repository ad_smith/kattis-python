import math


while True:
    line = input()
    if line == "0 0 0":
        break
    r, m, c = map(float, line.split())
    print(math.pi * r * r, (r * 2) ** 2 * c / m)
