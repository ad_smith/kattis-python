import sys


for i, line in enumerate(sys.stdin):
    if line.strip() == "END":
        break
    arr = []
    index = -1
    # Find all the positions of *
    while True:
        index = line.find("*", index + 1)
        if index == -1:
            break
        arr.append(index)
    # If the difference between all elements of arr is the same then even, else not even
    for j in range(len(arr) - 2):
        if arr[j + 2] - arr[j + 1] != arr[j + 1] - arr[j]:
            print(f"{i + 1} NOT EVEN")
            break
    else:
        print(f"{i + 1} EVEN")

