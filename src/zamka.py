import sys


def sum_digits(n):
    s = 0
    while n:
        s += n % 10
        n //= 10
    return s


low = int(sys.stdin.readline())
high = int(sys.stdin.readline())
x = int(sys.stdin.readline())

for i in range(low, high + 1):
    if sum_digits(i) == x:
        print(i)
        break

for j in reversed(range(low, high + 1)):
    if sum_digits(j) == x:
        print(j)
        break
