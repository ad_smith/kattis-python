import sys
from collections import defaultdict


s = sys.stdin.readline().strip()
cur = 0
moves = defaultdict(dict)
moves['A'][0] = 1
moves['A'][1] = 0
moves['B'][1] = 2
moves['B'][2] = 1
moves['C'][0] = 2
moves['C'][2] = 0

for move in list(s):
    if cur in moves[move]:
        cur = moves[move][cur]

print(cur + 1)
