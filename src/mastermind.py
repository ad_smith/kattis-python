import sys
from collections import defaultdict


n, a, b = sys.stdin.readline().split()
code_colors = defaultdict(int)
guess_colors = defaultdict(int)

r = 0
for c1, c2 in zip(a, b):
    if c1 == c2:
        r += 1
    else:
        code_colors[c1] += 1
        guess_colors[c2] += 1

s = 0
for code_color in code_colors:
    s += min(code_colors[code_color], guess_colors[code_color])

print(r, s)
