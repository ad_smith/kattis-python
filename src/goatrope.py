import math


x0, y0, x1, y1, x2, y2 = map(int, input().split())

if min(x1, x2) <= x0 <= max(x1, x2):
    print(min(abs(y0 - y1), abs(y0 - y2)))
elif min(y1, y2) <= y0 <= max(y1, y2):
    print(min(abs(x0 - x1), abs(x0 - x2)))
else:
    distance = min(math.sqrt(abs(x0 - x1) ** 2 + abs(y0 - y1) ** 2),
                   math.sqrt(abs(x0 - x1) ** 2 + abs(y0 - y2) ** 2),
                   math.sqrt(abs(x0 - x2) ** 2 + abs(y0 - y1) ** 2),
                   math.sqrt(abs(x0 - x2) ** 2 + abs(y0 - y2) ** 2))
    print(distance)
