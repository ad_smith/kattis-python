import sys


n = int(sys.stdin.readline())
e = 1
val = 1
for i in range(1, n + 1):
    val *= i
    e += 1 / val
print(e)
