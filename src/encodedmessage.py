import sys
import math


n = int(sys.stdin.readline())
for _ in range(n):
    s = sys.stdin.readline().strip()
    s_length = int(math.sqrt(len(s)))
    lst = [ch for ch in s]
    res = []
    for col in reversed(range(s_length)):
        for row in range(s_length):
            ch = s[s_length * row + col]
            res.append(ch)
    print(''.join(res))

