import sys


t = int(sys.stdin.readline())
for _ in range(t):
    n = int(sys.stdin.readline())
    divisor = 1
    while n % divisor == 0:
        divisor *= 10
    n -= divisor // 10
    print(n)

