import sys


word = sys.stdin.readline().strip()
s = {x for x in word}
letters = sys.stdin.readline().strip()
wrong = 0
for ch in letters:
    if ch in s:
        s.remove(ch)
        if len(s) == 0:
            print('WIN')
            break
    else:
        wrong += 1
        if wrong >= 10:
            print('LOSE')
            break


