import sys


def sum_digits(n):
    s = 0
    while n:
        s += n % 10
        n //= 10
    return s


while True:
    n = int(sys.stdin.readline())
    if not n:
        break
    s = sum_digits(n)
    for i in range(11, 100001):
        if sum_digits(n * i) == s:
            print(i)
            break

