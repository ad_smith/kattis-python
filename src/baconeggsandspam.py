import sys
from collections import defaultdict


while True:
    n = int(sys.stdin.readline())
    if n == 0:
        break

    d = defaultdict(set)
    for _ in range(n):
        lst = sys.stdin.readline().split()
        for i in range(1, len(lst)):
            item = lst[i]
            d[item].add(lst[0])

    for key in sorted(d.keys()):
        print(f'{key} {" ".join(sorted(d[key]))}')
    print()

