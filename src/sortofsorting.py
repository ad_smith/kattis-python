import sys


while True:
    n = int(sys.stdin.readline())
    if n == 0:
        break
    names = []
    for _ in range(n):
        names.append(sys.stdin.readline().strip())
    names.sort(key=lambda x: x[0:2])
    for name in names:
        print(name)
    print()  # Extra whitespace at end doesn't affect the result
