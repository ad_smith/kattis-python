s = input()
problems = s.split(';')
solve = 0
for p in problems:
    if '-' in p:
        a, b = map(int, p.split('-'))
        solve += b - a + 1
    else:
        solve += 1
print(solve)
