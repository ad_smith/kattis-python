import sys

n = int(sys.stdin.readline())
while True:

    list1 = []
    list2 = []
    for _ in range(n):
        list1.append(int(sys.stdin.readline()))
    for _ in range(n):
        list2.append(int(sys.stdin.readline()))

    orig = list1.copy()
    list1.sort()
    list2.sort()

    d = {}
    for i, j in zip(list1, list2):
        d[i] = j
    print('\n'.join([str(d[x]) for x in orig]))
    n = int(sys.stdin.readline())
    if n == 0:
        break
    else:
        print('')
