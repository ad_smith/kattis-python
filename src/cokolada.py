import sys


k = int(sys.stdin.readline())
original_bar_size = 1
while k > original_bar_size:
    original_bar_size *= 2

breaks = 0
current_bar_size = original_bar_size

while True:
    if current_bar_size > k:
        current_bar_size //= 2
        breaks += 1
        if current_bar_size < k:
            k -= current_bar_size
    elif current_bar_size == k:
        break

print(original_bar_size, breaks)
