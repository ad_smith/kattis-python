import sys


def gcd(a, b):
    if b == 0:
        return a
    return gcd(b, a % b)


sys.stdin.readline()
rings = [int(x) for x in sys.stdin.readline().split()]

for i in range(1, len(rings)):
    a = rings[0]
    b = rings[i]
    d = gcd(a, b)
    print(f'{a // d}/{b // d}')
