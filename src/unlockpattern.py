import math
import sys


def distance(x1, y1, x2, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


lines = []
for _ in range(3):
    lines.append([int(x) for x in sys.stdin.readline().split()])

positions = list(range(9))
for y in range(len(lines)):
    for x in range(3):
        positions[lines[y][x] - 1] = (x, y)

total_distance = 0
for i in range(8):
    total_distance += distance(*positions[i], *positions[i + 1])

print(total_distance)
