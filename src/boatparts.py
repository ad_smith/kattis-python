import sys


p, n = map(int, sys.stdin.readline().split())
s = set()
for i in range(n):
    s.add(sys.stdin.readline().strip())
    if len(s) == p:
        print(i + 1)
        break
else:
    print('paradox avoided')
