import sys


while True:

    n, m = map(int, sys.stdin.readline().split())
    if n == 0 and m == 0:
        break

    calls = []
    for _ in range(n):
        source, destination, start, duration = map(int, sys.stdin.readline().split())
        calls.append((start, start + duration))

    for _ in range(m):
        start, duration = map(int, sys.stdin.readline().split())
        active = 0
        for call in calls:
            # The call must end after the interval starts and start before the
            # interval ends
            if call[1] > start and call[0] < start + duration:
                active += 1
        print(active)
