import sys


k = int(sys.stdin.readline())
n = int(sys.stdin.readline())
t = 0

for _ in range(n):
    splt = sys.stdin.readline().split()
    t += int(splt[0])
    z = splt[1].strip()
    if t >= 210:
        break
    if z == 'T':
        k += 1
    if k > 8:
        k = 1
print(k)
