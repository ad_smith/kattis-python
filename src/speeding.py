import sys


sys.stdin.readline()
sys.stdin.readline()

last_time = 0
last_dist = 0
max_speed = 0

for line in sys.stdin:
    t, d = map(int, line.split())
    cur_speed = (d - last_dist) // (t - last_time)
    max_speed = max(max_speed, cur_speed)
    last_time = t
    last_dist = d

print(max_speed)
