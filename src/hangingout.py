import sys


limit, _ = map(int, sys.stdin.readline().split())
cur = 0
not_allowed = 0
for line in sys.stdin:
    action, n = line.split()
    n = int(n)
    if action == 'enter':
        if cur + n <= limit:
            cur += n
        else:
            not_allowed += 1
    else:
        cur -= n
print(not_allowed)
