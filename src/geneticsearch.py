import sys


def count_all(a, b):
    """
    Returns the count of all occurrences of a in b including overlapping.
    """
    count = 0
    start = 0
    while True:
        start = b.find(a, start) + 1
        if start > 0:
            count += 1
        else:
            return count


while True:
    line = sys.stdin.readline().strip()
    if line == '0':
        break

    a, b = line.split()
    t1 = count_all(a, b)

    t2_strings = set()
    for i in range(len(a)):
        t2_string = a[:i] + a[i + 1:]
        t2_strings.add(t2_string)

    t3_strings = set()
    for i in range(len(a) + 1):
        for c in ['A', 'G', 'C', 'T']:
            t3_string = a[:i] + c + a[i:]
            t3_strings.add(t3_string)

    t2 = sum(count_all(x, b) for x in t2_strings)
    t3 = sum(count_all(x, b) for x in t3_strings)

    print(t1, t2, t3)
