import sys


def find_match(a, b):
    for i, ch1 in enumerate(a):
        for j, ch2 in enumerate(b):
            if ch1 == ch2:
                return i, j


a, b = sys.stdin.readline().split()
col, row = find_match(a, b)

for i in range(len(b)):
    for j in range(len(a)):
        if j == col:
            print(b[i], end='')
        elif i == row:
            print(a[j], end='')
        else:
            print('.', end='')
    print()
