n = int(input())
total = 0
for _ in range(n):
    p = int(input())
    total += (p // 10) ** (p % 10)
print(total)

