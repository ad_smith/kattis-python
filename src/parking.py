import sys


cost = [0, *list(map(int, sys.stdin.readline().split()))]
arr = [0 for x in range(101)]

for _ in range(3):
    arrive, depart = map(int, sys.stdin.readline().split())
    for i in range(arrive, depart):
        arr[i] += 1

total_cost = 0
for cars in arr:
    total_cost += cost[cars] * cars
print(total_cost)
