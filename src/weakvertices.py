import sys


while True:
    n = int(sys.stdin.readline())
    if n == -1:
        break

    matrix = []
    for i in range(n):
        matrix.append([int(x) for x in sys.stdin.readline().split()])

    weak = []
    for i in range(n):
        is_weak = True
        for j in range(n):
            for k in range(n):
                if matrix[i][j] and matrix[i][k] and j != k and matrix[j][k]:
                    is_weak = False
        if is_weak:
            weak.append(i)

    print(' '.join([str(x) for x in weak]))
