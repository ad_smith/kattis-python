import sys


r, c, zr, zc = map(int, sys.stdin.readline().split())

for _ in range(r):
    line = sys.stdin.readline().strip()
    updated_line = ''
    for ch in line:
        updated_line += ch * zc
    for _ in range(zr):
        print(updated_line)
